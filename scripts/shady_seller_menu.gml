/// shady_seller_menu()

add_message("You see a shady seller. You can instantly tell what this guy does for a living. What do you want to do?");
    
add_option(OD_BLACK_MARKET.LEAVE,       "Walk away");
add_option(OD_BLACK_MARKET.BUY,         "Ask if they have anything you can buy");
add_option(OD_BLACK_MARKET.SELL,        "Show them what you have to sell");
