#define dialogue_scripts

    /*
    These are the dialogue scripts !!!
    */
    
enum OPTIONS
{
    ID,                 // unique id during a dialogue
    TXT,                // text to display
    AVA,                // whether or not its available to select
    
    CONSUMABLE_ITEM,    // the item to be consumed if this option is chosen
    CONSUMABLE_QUAN,    // the quantity to be consumed
};

#define open_dialogue
///open_dialogue(dialogue, speaker[optional])

current_dialogue = instance_create(0, 0, argument[0]);
current_dialogue.owner = id;

if (argument_count > 1)
    current_dialogue.speaker = argument[1];

with (current_dialogue)
    event_user(USER_EVENTS.SETUP);

#define close_dialogue
///close_dialogue()

owner.dialogue_just_closed = true;
owner.current_dialogue = undefined;
instance_destroy();

#define clear_options
///clear_options()

max_options =           0;
options =               0;
option_pick_counts =    0;

#define add_option
/// add_option(id, text, onetime, stat_id, stat_val, consume_item_id, consume_val, require_item_id)

var unique_option_id =  argument[0];
var text =              argument[1];
var condition =         true;
var consumable =        undefined;
var consumable_amount = undefined;

// one time use check
if (condition and argument_count > 2 and argument[2] != undefined and argument[2] == true)
    condition = option_pick_count(unique_option_id) == 0;

// stat required check
if (condition and argument_count > 4 and argument[3] != undefined and argument[4] != undefined)
    condition = obj_player.stats[argument[3]] >= argument[4];

// consumable item check
if (condition and argument_count > 6 and argument[5] != undefined and argument[6] != undefined)
{
    consumable = argument[5];
    consumable_amount = argument[6];
    condition = has_item(consumable, consumable_amount);
}

// item check
if (condition and argument_count > 7 and argument[7] != undefined)
    condition = has_item(argument[7]);

if (condition or global.grey_non_options)
{
    var curr_size = 0;

    if (is_undefined(options) == false and is_array(options))
        curr_size = array_height_2d(options);

    options[curr_size, OPTIONS.ID] =                argument[0];
    options[curr_size, OPTIONS.TXT] =               argument[1];
    options[curr_size, OPTIONS.AVA] =               condition;
    options[curr_size, OPTIONS.CONSUMABLE_ITEM] =   consumable;
    options[curr_size, OPTIONS.CONSUMABLE_QUAN] =   consumable_amount;
}

#define add_message
/// add_message(msg)

display_message = argument0;

#define setup_option_selection
/// setup_option_selection()

available_options = 0;
total_options =     0;
no_options =        is_array(options) == false or array_height_2d(options) == 0;

if (no_options)
    add_option(0, "Continue Travelling");

total_options = array_height_2d(options);

for (var i = 0; i < total_options; ++i)
    if (options[i, OPTIONS.AVA])
        ++available_options;
        
if (select != -1 and select < total_options)
{
    selected = select;
    select = -1;
}
else
    selected = 0;

#define start_dialogue
///start_dialogue()

event_inherited();

#define end_dialogue
///end_dialogue()

event_user(DIALOGUE_USER_EVENTS.POST_SETUP);

#define pass_dialogue
///pass_dialogue(dialogue)

with (instance_create(0, 0, argument0))
{
    owner = other.owner;
    owner.current_dialogue = id;
    speaker = other.speaker;

    event_user(USER_EVENTS.SETUP);
}

instance_destroy();
#define travel_menu
/// travel_menu()

add_message("You've stopped. What would you like to do?");
    
add_option(OD_STOP_TRAVEL.CONTINUE,     "Continue travelling");

if (obj_player.day)
    add_option(OD_STOP_TRAVEL.REST,     "Rest until night");

if (obj_player.night)
    add_option(OD_STOP_TRAVEL.SLEEP,    "Sleep until morning");

add_option(OD_STOP_TRAVEL.SUPPLIES,     "Check supplies");
add_option(OD_STOP_TRAVEL.MAP,          "Check map");
add_option(OD_STOP_TRAVEL.HOSTAGES,     "Check hostages");

#define landmark_menu
/// landmark_menu()

add_message("What would you like to do?");
    
add_option(OD_STOP_LANDMARK.CONTINUE,   "Continue looking around");
add_option(OD_STOP_LANDMARK.SUPPLIES,   "Check supplies");
add_option(OD_STOP_LANDMARK.MAP,        "Check map");

#define set_selection
/// set_selection(selection)

var size = array_height_2d(options);

for (var i = 0; i < size; ++i)
    if (argument0 == options[i, OPTIONS.ID])
    {
        select = i;
        exit;
    }

#define option_pick_count
/// option_pick_count(option_id)

if (is_array(option_pick_counts) == false or array_length_1d(option_pick_counts) <= argument0)
    option_pick_counts[argument0] = 0;

return option_pick_counts[argument0];