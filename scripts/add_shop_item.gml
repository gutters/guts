/// add_shop_item(item_id, price, items_per_purchase)

var size = 0;

if (is_array(items_for_sale) or items_for_sale != undefined)
    size = array_height_2d(items_for_sale);
    
items_for_sale[size, SHOP_ITEM.ID] =        argument0;
items_for_sale[size, SHOP_ITEM.PRICE] =     argument1;
items_for_sale[size, SHOP_ITEM.IPP] =       argument2;

// unused now
items_for_sale[size, SHOP_ITEM.AVAILABLE] = 0;
