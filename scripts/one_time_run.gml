///one_time_run()

global.grey_non_options =       true;

randomize();

enum USER_EVENTS                { SETUP };
enum FLIP                       { LEFT = -1, RIGHT = 1 };
enum GAMEOVER_TYPE              { CAUGHT, KILLED };

enum THEME
{
    MAIN_MENU_SCREEN =          $716999,
    OPTIONS =                   $716999,
    DIALOGUE_BOX_TEXT =         $FFFFFF,
    SELECTED_COLOUR =           $5CB87A,
    UNAVAILABLE_COLOUR =        $6A6AD4,
    HUNTING_BG_COLOUR =         $2F694B,
    SKY_DAY =                   $E5CD5F,
    SKY_NIGHT =                 $743F3F,
};

// disable automatic drawing, we will specify it manually
// incase we decide to have any shader effects
application_surface_draw_enable(false);

// create the items
create_item_db();