#define bordered_box_scripts

    /*
    These are the bordered box scripts !!!
    */

#define create_bordered_box
///create_bordered_box(width, height, sprite, bordercolour, bgcolour, bordersize)

var boxid = ds_list_create();

enum __BB { SPRITE, BW, BH, BORDER_COL, BG_COL, BS, W, H, B, R, SX, SY, SBGX, SBGY };

var width =                 argument0;
var height =                argument1;

boxid[| __BB.SPRITE] =      argument2;
boxid[| __BB.BORDER_COL] =  argument3;
boxid[| __BB.BG_COL] =      argument4;
boxid[| __BB.BS] =          argument5;

boxid[| __BB.W] =           sprite_get_width(argument2);
boxid[| __BB.H] =           sprite_get_height(argument2);

boxid[| __BB.B] =           height - 8;
boxid[| __BB.R] =           width - 8;

boxid[| __BB.SX] =          (width-16)/8;
boxid[| __BB.SY] =          (height-16)/8;
boxid[| __BB.SBGX] =        (width-(argument5*2))/8;
boxid[| __BB.SBGY] =        (height-(argument5*2))/8;

boxid[| __BB.BW] =          width;
boxid[| __BB.BH] =          height;

return boxid;
#define destroy_bordered_box
///destroy_bordered_box(boxid)

ds_list_destroy(argument0);

#define draw_bordered_box
///draw_bordered_box(boxid, x, y)

var boxid = argument0;
var l =     argument1;
var t =     argument2;

var spr =   boxid[| __BB.SPRITE];
var w =     boxid[| __BB.W];
var h =     boxid[| __BB.H];
var bs =    boxid[| __BB.BS];
var bc =    boxid[| __BB.BORDER_COL];
var b =     t + boxid[| __BB.B];
var r =     l + boxid[| __BB.R];
var sx =    boxid[| __BB.SX];
var sy =    boxid[| __BB.SY];

var a =     draw_get_alpha();

draw_sprite_part_ext(spr, 0, 8, 8, 8, 8, l+bs, t+bs, boxid[| __BB.SBGX], boxid[| __BB.SBGY], boxid[| __BB.BG_COL], a);  // background

draw_sprite_part_ext(spr, 0, 0,    0,    8, 8, l, t, 1, 1, bc, a);      // top left corner
draw_sprite_part_ext(spr, 0, 0,    h-8,  8, 8, l, b, 1, 1, bc, a);      // bottom left corner
draw_sprite_part_ext(spr, 0, w-8,  0,    8, 8, r, t, 1, 1, bc, a);      // top right corner
draw_sprite_part_ext(spr, 0, w-8,  h-8,  8, 8, r, b, 1, 1, bc, a);      // bottom right corner

draw_sprite_part_ext(spr, 0, 0,    8,    8, 8, l, t+8, 1,  sy, bc, a);  // left 
draw_sprite_part_ext(spr, 0, w-8,  8,    8, 8, r, t+8, 1,  sy, bc, a);  // right
draw_sprite_part_ext(spr, 0, 8,    0,    8, 8, l+8, t, sx, 1,  bc, a);  // top
draw_sprite_part_ext(spr, 0, 8,    h-8,  8, 8, l+8, b, sx, 1,  bc, a);  // bottom

#define get_bordered_box_height
///get_bordered_box_height(boxid)

return argument0[| __BB.BH];

#define get_bordered_box_width
///get_bordered_box_width(boxid)

return argument0[| __BB.BW];