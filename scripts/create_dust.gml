/// create_dust()

var tlx = x - sprite_xoffset;
var tly = y - sprite_yoffset;

for (var i = 0; i < 3; ++i)
    with (instance_create(tlx + irandom(sprite_width), tly + irandom(sprite_height), obj_smoke))
    {
        sprite_index =  spr_smoke;
        vspeed =        -0.1;
        decay_speed =   0.1;
    }