#define hunting_scripts

    /*
    These are the hunting scripts !!!
    */

#define add_hunting_subject
/// add_hunting_subject(subject_id)

obj_player.hunting[array_length_1d(obj_player.hunting)] = argument0;

#define clear_hunting_subjects
/// clear_hunting_subjects

obj_player.hunting = 0;
