#define player_scripts

    /*
    These are the player scripts !!!
    */
    
enum INV_ITEM   { ID, QUANTITY, COUNT };
enum STAT       { STRENGTH, CHARISMA, STEALTH, TECHNIQUE };
enum HOSTAGE    { NAME, TYPE, COUNT };

#define skip_time
///skip_time(min, hours[optional])

with (obj_player)
{
    time_of_day_m += argument[0];
    
    if (argument_count > 1)
        time_of_day_h += argument[1];
    
    while (time_of_day_m >= 60)
    {
        time_of_day_m -= 60;
        ++time_of_day_h;
    }
    
    while (time_of_day_h >= 24)
    {
        time_of_day_h -= 24;
        ++total_days_travelled;
    }
    
    day = (time_of_day_h > 6 and time_of_day_h < 22);
    night = !day;
}

#define pickup_item
/// pickup_item(item_id, quantity[optional])

var item_id =   argument[0];
var quantity =  1;

    
if (argument_count > 1)
    quantity = argument[1];

if (has_item(item_id))
{
    var inv =       obj_player.inventory;
    var inv_size =  obj_player.inventory_size;

    for (var i = 0; i < inv_size; ++i)
    {
        if (inv[# i, INV_ITEM.ID] == item_id)
        {
            inv[# i, INV_ITEM.QUANTITY] += quantity;
            return true;
        }
    }
}
else
{
    ++obj_player.items_in_inventory;

    if (free_inventory_space() <= 0)
    {
        ++obj_player.inventory_size;
        ds_grid_resize(obj_player.inventory, obj_player.inventory_size, INV_ITEM.COUNT);
        obj_player.inventory[# obj_player.inventory_size - 1, INV_ITEM.ID] =        item_id;
        obj_player.inventory[# obj_player.inventory_size - 1, INV_ITEM.QUANTITY] =  quantity;
        return true;
    }
    
    var inv =       obj_player.inventory;
    var inv_size =  obj_player.inventory_size;

    for (var i = 0; i < inv_size; ++i)
    {
        if (inv[# i, INV_ITEM.ID] == NO_ITEM)
        {
            inv[# i, INV_ITEM.ID] =         item_id;
            inv[# i, INV_ITEM.QUANTITY] =   quantity;
            return true;
        }
    }
}

return false;

#define has_item
/// has_item(item_id, amount[optional])

var item_id =   argument[0];
var inv =       obj_player.inventory;
var inv_size =  obj_player.inventory_size;
var amount =    1;

if (argument_count > 1)
    amount = argument[1];

for (var i = 0; i < inv_size; ++i)
{
    if (inv[# i, INV_ITEM.ID] == item_id)
        return inv[# i, INV_ITEM.QUANTITY] >= amount;
}

return false;

#define item_count
/// item_count(item_id)

var item_id =   argument[0];
var inv =       obj_player.inventory;
var inv_size =  obj_player.inventory_size;

for (var i = 0; i < inv_size; ++i)
{
    if (inv[# i, INV_ITEM.ID] == item_id)
        return inv[# i, INV_ITEM.QUANTITY];
}

return 0;

#define drop_item
/// drop_item(item_id, quantity[optional])

var item_id =   argument[0];
var inv =       obj_player.inventory;
var inv_size =  obj_player.inventory_size;
var amount =    1;

if (argument_count > 1)
    amount = argument[1];
    
for (var i = 0; i < inv_size; ++i)
{
    if (inv[# i, INV_ITEM.ID] == item_id)
    {
        if (inv[# i, INV_ITEM.QUANTITY] < amount)
            return false;
            
        inv[# i, INV_ITEM.QUANTITY] -= amount;
        
        if (inv[# i, INV_ITEM.QUANTITY] == 0)
        {
            inv[# i, INV_ITEM.ID] = NO_ITEM;
            --obj_player.items_in_inventory;
        }

        return true;
    }
}

return false;

#define free_inventory_space
/// free_inventory_space()

return obj_player.inventory_size - obj_player.items_in_inventory;

#define add_new_hostage
/// add_new_hostage(name)

with (speaker)
{
    create_dust();
    instance_destroy();
}

with (obj_player)
{
    if (current_hostage_count < hostages_size)
        for (var i = 0; i < hostages_size; ++i)
            if (hostages[# i, HOSTAGE.NAME] == undefined)
            {
                hostages[# i, HOSTAGE.NAME] = argument0;
                hostages[# i, HOSTAGE.TYPE] = other.id;
                ++current_hostage_count;
                return true;
            }
}

return false;