#define text_scripts

    /*
    These are scripts for drawing text in different ways !!!
    */

#define draw_text_outline_color
///draw_text_outline_color(x, y, outlinesize, text, textcolour, outlinecolour)

var starting_color = draw_get_color();

draw_set_color(argument5);

var tim = argument0 + argument2;
var tnl = argument1 + argument2;

for (var ti = (argument0 - argument2); ti <= tim; ++ti)
    for (var tn = (argument1 - argument2); tn <= tnl; ++tn)
        draw_text(ti, tn, argument3);

draw_set_color(argument4);
draw_text(argument0, argument1, argument3);

draw_set_color(starting_color);