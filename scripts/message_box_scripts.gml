#define message_box_scripts

    /*
    These are the message box scripts !!!
    */

#define create_message_box
/// create_message_box(width, height)

with (instance_create(room_width / 2 - argument0 / 2, room_height / 2 - argument1 / 2, obj_msg_box))
{
    width =  argument0;
    height = argument1;
    event_user(USER_EVENTS.SETUP);
    return id;
}

#define show_message_box
/// show_message_box(id, msg, type[optional], default_selection[optional])

with (argument[0])
{
    message = argument[1];
    
    if (argument_count > 2)
        type = argument[2];
        
    if (argument_count > 3)
        selection = argument[3];

    show_msg_box = true;
}

#define is_awaiting_input
/// is_awaiting_input(id)

return argument0.visible;

#define get_msg_box_result
/// get_msg_box_result(id)

with (argument0)
    if (visible == false)
    {
        var _result = result;
        result = MESSAGE_BOX_RESULT.WAITING;
        return _result;
    }

return MESSAGE_BOX_RESULT.WAITING;