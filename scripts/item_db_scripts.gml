#define item_db_scripts

    /*
    These are the item database scripts !!!
    */

#define create_item_db
/// create_item_db()

enum ITEM_INFO { SPRITE, NAME, DESC, VALUE };

global.item_db = ds_map_create();

add_item_to_db(NO_ITEM,             "Empty",            "",                         0);

add_item_to_db(spr_gun_ammo,        "Gun Ammo",         "Ammo for a gun.",          1);

add_item_to_db(spr_van_keys,        "Van Keys",         "Keys for a van vehicle.",  0);
add_item_to_db(spr_coat_hanger,     "Coat Hanger",      "Cheap wire coat hanger.",  1);


add_item_to_db(spr_banana,          "Banana",           "Tasty Banana.",            1);
add_item_to_db(spr_rope,            "Rope",             "Some strong rope.",        5);
add_item_to_db(spr_license_plate,   "License Plate",    "Use to replace your license plates and reduce heat from the cops.", 50);

// TODO: add items...

#define destroy_item_db
/// destroy_item_db()

// TODO: cycle map, destroy items
// ...

ds_map_destroy(global.item_db);

#define add_item_to_db
/// add_item_to_db(sprite, name, desc, sell_value)

var item = ds_list_create();

item[| ITEM_INFO.SPRITE] =      argument[0];
item[| ITEM_INFO.NAME] =        argument[1];
item[| ITEM_INFO.DESC] =        argument[2];
item[| ITEM_INFO.VALUE] =       argument[3];

global.item_db[? argument[0]] = item;

return item;

#define get_item_info
/// get_item_info(item_id, info)

var item = global.item_db[? argument0];

return item[| argument1];